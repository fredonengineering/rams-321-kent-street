# RAMS Head Office Repository #

## Level 12, 321 Kent Street ##



### Recent changes ###

Unified Interface 1.1

* Incorporates extra pages for Fusion bidirectional support communications
* Added MagicInfo as available source in Advanced Menus

Programming 1.3

* Requested changes by the client to add in an auto shutdown timer for the Projector room

### Changelog ###

Programming 1.2 

* Improved Fusion control and reporting
* MagicInfo source available in advanced pages

Programming 1.1

* Preliminary implemetation of Airmedia control
* Added combination functionality of Kitchen and Breakout

Programming 1.0 - ready for UAT

* Advanced menu trigger now not triggered by hardbuttons
* Volume buttons now unmute volume if muted
* Advanced DM info now should work properly for all spaces
* Basic Cisco SX80 control implemented
* Added Fire Trip logic and DSP mute control

Programming 0.9

* Preliminary implementation of Fusion control and reporting
* All Advanced Menu functions have been programmed

Programming 0.7

* DSP control has been implemented and working
* Some Advanced Menu functions are now working

Programming 0.6

* Display control implemented for all rooms
* DM routing and feedback implemented
* Subject to testing and debugging

Programming 0.5

* Interface logic completed for all rooms